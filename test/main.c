/*
 ============================================================================
 Name        : MakeDemo testing
 Author      : Zamek
 Version     :
 Copyright   : GPL
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "../include/globals.h"
#include "../include/ma.h"
#include "../include/mb.h"
#include "../include/mc.h"
#include "../include/md.h"

int init_tests(void) {
	printf("01\n");
	int mai=module_a_init();
	printf("mai:%d\n",mai);

	CU_ASSERT_EQUAL(mai, EXIT_SUCCESS);
	printf("02\n");
	CU_ASSERT_EQUAL(module_b_init(), EXIT_SUCCESS);
	printf("03\n");
	CU_ASSERT_EQUAL(module_c_init(), EXIT_SUCCESS);
	printf("04\n");
	CU_ASSERT_EQUAL(module_d_init(), EXIT_SUCCESS);
	printf("05\n");
	return 0;
}

void process_tests(void) {
	printf("10\n");
	CU_ASSERT_EQUAL(module_a_process(), EXIT_SUCCESS);
	printf("11\n");
	CU_ASSERT_EQUAL(module_b_process(), EXIT_SUCCESS);
	printf("12\n");
	CU_ASSERT_EQUAL(module_c_process(), EXIT_SUCCESS);
	printf("13\n");
	CU_ASSERT_EQUAL(module_d_process(), EXIT_SUCCESS);
}

int close_tests(void) {
	CU_ASSERT_EQUAL(module_a_close(), EXIT_SUCCESS);
	CU_ASSERT_EQUAL(module_b_close(), EXIT_SUCCESS);
	CU_ASSERT_EQUAL(module_c_close(), EXIT_SUCCESS);
	CU_ASSERT_EQUAL(module_d_close(), EXIT_SUCCESS);
	return 0;
}

int main(void) {
	CU_pSuite pSuite=NULL;

	if (CUE_SUCCESS!=CU_initialize_registry())
		return (CU_get_error());

	printf("1\n");
	pSuite=CU_add_suite("Suite 1", 0,0);
	if (NULL==pSuite) {
		CU_cleanup_registry();
		return (CU_get_error());
	}

	printf("2\n");
	if ((NULL==CU_add_test(pSuite, "Init test", init_tests)) ||
		(NULL==CU_add_test(pSuite, "Process test", process_tests)) ||
		(NULL==CU_add_test(pSuite, "Close test", close_tests))) {
		CU_cleanup_registry();
		return (CU_get_error());
	}

	printf("3\n");
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return (CU_get_error());
}
