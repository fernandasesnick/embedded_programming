/**
Module D
*/


/**
 * init module D
 *
 * \callgraph
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_d_init();

/**
 * Close module D
 *
 * free allocated memories
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_d_close();

/**
 * processing by module D
 *
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_d_process();


