/**
Module C
*/


/**
 * init module C
 *
 * \callgraph
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_c_init();

/**
 * Close module C
 *
 * free allocated memories
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_c_close();

/**
 * processing by module C
 *
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_c_process();


