# Makefile for makedemo

bin/Debug/P3_MakeDemo: bin bin/Debug bin/obj bin/obj/ma.o bin/obj/mb.o bin/obj/mc.o bin/obj/md.o bin/obj/main.o
	gcc -o bin/Debug/P3_MakeDemo bin/obj/ma.o bin/obj/mb.o bin/obj/mc.o bin/obj/md.o bin/obj/main.o -Wall -g -pedantic --std=c99
	bin/Debug/P3_MakeDemo

bin/obj: bin/
	mkdir bin/obj

bin/Debug: bin/
	mkdir bin/Debug
bin:
	mkdir bin

	
bin/obj/ma.o: src/ma.c
	gcc -I include -c src/ma.c -o bin/obj/ma.o -Wall -g -pedantic --std=c99 

bin/obj/mb.o: src/mb.c
	gcc -I include -c src/mb.c -o bin/obj/mb.o -Wall -g -pedantic --std=c99

bin/obj/mc.o: src/mc.c
	gcc -I include -c src/mc.c -o bin/obj/mc.o -Wall -g -pedantic --std=c99

bin/obj/md.o: src/md.c
	gcc -I include -c src/md.c -o bin/obj/md.o -Wall -g -pedantic --std=c99

bin/obj/main.o: src/main.c
	gcc -I include -c src/main.c -o bin/obj/main.o -Wall -g -pedantic --std=c99
	
clean:
	rm -rf bin/Debug/P3_MakeDemo
	rm -rf bin/obj/ma.o
	rm -rf bin/obj/mb.o
	rm -rf bin/obj/mc.o
	rm -rf bin/obj/md.o
	rm -rf bin/obj/main.o
	rm -rf bin/obj
	rm -rf bin

debug_off:
	sed -i -e 's/#define DEBUG/#undef DEBUG/g' include/globals.h

debug_on:
	sed -i -e 's/#undef DEBUG/#define DEBUG/g' include/globals.h

dochtml:doc
	cd doc; doxygen

docclean:
	rm -rf doc/html

doc:
	mkdir doc
 
test: bin bin/Debug/test bin/obj bin/obj/ma.o bin/obj/mb.o bin/obj/mc.o bin/obj/md.o bin/Debug/test
	bin/Debug/test

bin/Debug/test:test/main.o
	gcc -l cunit -o bin/Debug/test bin/obj/ma.o bin/obj/mb.o bin/obj/mc.o bin/obj/md.o test/main.o -Wall -g -pedantic --std=c99

test/main.o: test/main.c
	gcc -I include -c test/main.c -o test/main.o -Wall -g -pedantic --std=c99

testclean:
	rm test/main.o
	rm bin/exec/test

.PHONY: test
