/**
*/

#include <stdio.h>
#include <stdlib.h>
#include <md.h>
#include <globals.h>

/**
 * Debug this module
 */
#ifdef DEBUG
#define DEBUG_D
#else
#undef DEBUG_D
#endif

int module_d_init() {
#ifdef DEBUG_D
    printf("Enter module D init\n");
#endif
    return (EXIT_SUCCESS);
}

int module_d_close() {
#ifdef DEBUG_D
    printf("Enter module D close\n");
#endif
    return (EXIT_SUCCESS);
}

int module_d_process() {
#ifdef DEBUG_D
    printf("Enter module D process\n");
#endif
    return (EXIT_SUCCESS);
}

