/** Module A
*/

#include <stdio.h>
#include <stdlib.h>
#include <ma.h>
#include <globals.h>

/**
 * Debug this module
 */
#ifdef DEBUG
#define DEBUG_A
#else
#undef DEBUG_A
#endif

int module_a_init() {
#ifdef DEBUG_A
    printf("Enter module A init\n");
#endif
    
    return (EXIT_SUCCESS);
}

int module_a_close() {
#ifdef DEBUG_A
    printf("Enter module A close\n");
#endif
    return (EXIT_SUCCESS);
}

int module_a_process() {
#ifdef DEBUG_A
    printf("Enter module A process\n");
#endif
    return (EXIT_SUCCESS);
}

