/**
*/

#include <stdio.h>
#include <stdlib.h>
#include <mc.h>
#include <globals.h>

/**
 * Debug this module
 */
#ifdef DEBUG
#define DEBUG_C
#else
#undef DEBUG_C
#endif


int module_c_init() {
#ifdef DEBUG_C
    printf("Enter module C init\n");
#endif
    return (EXIT_SUCCESS);
}

int module_c_close() {
#ifdef DEBUG_C
    printf("Enter module C close\n");
#endif
    return (EXIT_SUCCESS);
}

int module_c_process() {
#ifdef DEBUG_C
    printf("Enter module C process\n");
#endif
    return (EXIT_SUCCESS);
}

